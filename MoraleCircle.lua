MoraleCircle = {}
local version = "1.2.7"
local MoralePerC = 0
local FillStart = 130
local FillEnd = 280

function MoraleCircle.init()

	MoraleCircle.Flash = {}
	local txtr, x, y, disabledTexture = GetIconData (005118)	
	for i=1,4 do
	
		CreateWindowFromTemplate ("MoraleWindow"..i, "MoraleTemplate", "Root")
		CircleImageSetTexture ("MoraleWindow"..i.."Icon", txtr, 32, 32)
		CircleImageSetTexture ("MoraleWindow"..i.."BG", txtr, 32, 32)
		CircleImageSetTexture ("MoraleWindow"..i.."IconBG", txtr, 32, 32)
		WindowSetTintColor( "MoraleWindow"..i.."BG", 0,0,0 )
		WindowSetTintColor( "MoraleWindow"..i.."IconBG", 0,0,0 )
		AnimatedImageStartAnimation ("MoraleWindow"..i.."Glow", 0, true, true, 0)	

		LayoutEditor.RegisterWindow( "MoraleWindow"..i, L"Morale "..towstring(i), L"Morale "..towstring(i), true, true, true, nil )	
		MoraleCircle.Flash[i] = false
	end

	RegisterEventHandler (SystemData.Events.PLAYER_MORALE_UPDATED,          "MoraleCircle.OnMoraleUpdated")

	MoraleCircle.FillColor = {R=25,G=255,B=25}
	MoraleCircle.FullColor = {R=50,G=75,B=255}
	MoraleCircle.EmptyColor = {R=155,G=20,B=20}

	MoraleCircle.M_Counter = {}
	MoraleCircle.M_Counter[1] = 10
	MoraleCircle.M_Counter[2] = 20
	MoraleCircle.M_Counter[3] = 50
	MoraleCircle.M_Counter[4] = 100

	TextLogAddEntry("Chat", 0, L"<icon57> MoraleCircle "..towstring(version)..L" Loaded.")
	
	
end

function MoraleCircle.update()
	for q=1,4 do
		local _, currentlySlottedMoraleAbilityId = GetMoraleBarData (q)
		local AbilityIcon = GetAbilityData(currentlySlottedMoraleAbilityId).iconNum
		local abilityId = GetAbilityData(currentlySlottedMoraleAbilityId).id
		local texture, x, y, disabledTexture = GetIconData (AbilityIcon)

		WindowSetTintColor("MoraleWindow"..q.."AbilityCooldown",10,0,0)
		WindowSetAlpha("MoraleWindow"..q.."AbilityCooldown",0.7)
		WindowSetShowing("MoraleWindow"..q,true) 	
	
		local M_Cdown,M_MaxCdown = GetMoraleCooldown (q)		
		CircleImageSetTexture ("MoraleWindow"..q.."Icon", texture, 32, 32)

		if (texture == nil or texture == "icon000000") and SystemData.MouseOverWindow.name ~= "MoraleWindow"..q then
			WindowSetShowing("MoraleWindow"..q,false) 	
--			WindowSetAlpha("MoraleWindow"..q,0)
			WindowSetShowing("MoraleWindow"..q.."MoraleLabel" ,false) 	
			WindowSetShowing("MoraleWindow"..q.."MoraleLabelBG" ,false) 				
		else 
--			WindowSetAlpha("MoraleWindow"..q,1) 
			WindowSetShowing("MoraleWindow"..q,true) 			
			WindowSetShowing("MoraleWindow"..q.."MoraleLabel" ,true) 
			WindowSetShowing("MoraleWindow"..q.."MoraleLabelBG" ,true) 			
		end
				
		if M_Cdown >= ActionButton.GLOBAL_COOLDOWN then	
			CooldownDisplaySetCooldown( "MoraleWindow"..q.."AbilityCooldown", M_Cdown,60)
			LabelSetText("MoraleWindow"..q.."CooldownLabel",towstring(TimeUtils.FormatSeconds(M_Cdown, true)))	
		else
			CooldownDisplaySetCooldown( "MoraleWindow"..q.."AbilityCooldown", M_Cdown, M_MaxCdown)
			LabelSetText("MoraleWindow"..q.."CooldownLabel",L"")
		end	
		
		
		if MoralePerC >= MoraleCircle.M_Counter[q] then

			CircleImageSetFillParams( "MoraleWindow"..q.."Empty", FillStart,  FillEnd) 
			WindowSetTintColor( "MoraleWindow"..q.."Empty", MoraleCircle.EmptyColor.R,MoraleCircle.EmptyColor.G,MoraleCircle.EmptyColor.B )

			CircleImageSetFillParams( "MoraleWindow"..q.."FillBG", FillStart,  FillEnd) 
			WindowSetTintColor( "MoraleWindow"..q.."FillBG", 0,0,0 )
			CircleImageSetFillParams( "MoraleWindow"..q.."Fill", FillStart,  FillEnd) 
			WindowSetTintColor( "MoraleWindow"..q.."Fill", MoraleCircle.FullColor.R,MoraleCircle.FullColor.G,MoraleCircle.FullColor.B )
			LabelSetText("MoraleWindow"..q.."MoraleLabel",L"100%")
			LabelSetText("MoraleWindow"..q.."MoraleLabelBG",L"100%")
			WindowSetAlpha("MoraleWindow"..q.."Glow",1)
			LabelSetTextColor("MoraleWindow"..q.."MoraleLabel",155,155,255)			
			
			if WindowGetShowing("MoraleWindow"..q.."Flash") == false and MoraleCircle.Flash[q] == false then
				MoraleCircle.Flash[q] = true
				WindowSetShowing("MoraleWindow"..q.."Flash",true)
				WindowSetAlpha("MoraleWindow"..q.."Flash",1)
				AnimatedImageStartAnimation ("MoraleWindow"..q.."Flash", 0, false, true, 0)
			end
   
		elseif MoralePerC <= MoraleCircle.M_Counter[q] then

			MoraleCircle.Flash[q] = false
			WindowSetShowing("MoraleWindow"..q.."Flash",false)
			WindowSetAlpha("MoraleWindow"..q.."Flash",0)
			AnimatedImageStopAnimation ("MoraleWindow"..q.."Flash")

			local startFill = 280 * (1 - (MoralePerC / GetMoralePercentForLevel(q)))

			CircleImageSetFillParams( "MoraleWindow"..q.."Empty", FillStart,  FillEnd) 
			WindowSetTintColor( "MoraleWindow"..q.."Empty",  MoraleCircle.EmptyColor.R,MoraleCircle.EmptyColor.G,MoraleCircle.EmptyColor.B )
			CircleImageSetFillParams( "MoraleWindow"..q.."FillBG", FillStart,  FillEnd - (startFill-4)) 
			WindowSetTintColor( "MoraleWindow"..q.."FillBG",0,10,20 )
			CircleImageSetFillParams( "MoraleWindow"..q.."Fill", FillStart,  FillEnd - startFill ) 
			WindowSetTintColor( "MoraleWindow"..q.."Fill",MoraleCircle.FillColor.R,MoraleCircle.FillColor.G,MoraleCircle.FillColor.B )
			LabelSetText("MoraleWindow"..q.."MoraleLabel",towstring(math.abs(MoralePerC/GetMoralePercentForLevel (q))*100)..L"%")
			LabelSetText("MoraleWindow"..q.."MoraleLabelBG",towstring(math.abs(MoralePerC/GetMoralePercentForLevel (q))*100)..L"%")
			WindowSetAlpha("MoraleWindow"..q.."Glow",0)
			LabelSetTextColor("MoraleWindow"..q.."MoraleLabel",255,255,255)			

		end
		
		local isBlocked = Player.IsAbilityBlocked( abilityId, GameData.AbilityType.MORALE )
		local isTargetValid, hasRequiredUnitTypeTargeted = IsTargetValid (abilityId)        
		local isTargetValid = (isTargetValid or (hasRequiredUnitTypeTargeted == false))
		
		if (isTargetValid and not isBlocked) then			
			WindowSetTintColor( "MoraleWindow"..q.."Icon", 255,255,255 )            
		else              
			WindowSetTintColor( "MoraleWindow"..q.."Icon", 255,0,0 )
		end				
	end 
end

function MoraleCircle.OnMouseOverStart(flags)
	local MouseOverWindowName = SystemData.MouseOverWindow.name
	local anchor = nil
    if  DoesWindowExist( "MouseOverTargetWindow" )and ( SystemData.Settings.GamePlay.staticAbilityTooltipPlacement or SystemData.Settings.GamePlay.staticTooltipPlacement ) then
        anchor = Tooltips.ANCHOR_MOUSE_OVER_TARGET_WINDOW
    else
        anchor = topleft
    end

	for i=1,4 do
		if MouseOverWindowName == "MoraleWindow"..i then
			if not (flags == SystemData.ButtonFlags.SHIFT) then
				WindowSetMovable( "MoraleWindow"..i, false )
				local _, currentlySlottedMoraleAbilityId = GetMoraleBarData (i)
				local abilityId = GetAbilityData(currentlySlottedMoraleAbilityId).id
				local MData = Player.GetAbilityData (GetAbilityData(currentlySlottedMoraleAbilityId).id, Player.AbilityType.MORALE)
				Tooltips.CreateAbilityTooltip( MData, SystemData.MouseOverWindow.name, anchor,L"<icon49>MoraleCircle",{ r = 50, g = 150, b =255 }) 
			else
				WindowSetMovable( "MoraleWindow"..i, true )
			end
		end
	end
end


function MoraleCircle.Click(flags)
	local MouseOverWindowName = SystemData.MouseOverWindow.name
	for i=1,4 do
		if MouseOverWindowName == "MoraleWindow"..i then
			if  flags == SystemData.ButtonFlags.SHIFT then
				WindowSetGameActionData( "MoraleWindow"..i, 0, 0, L"" )
			else
				local _, currentlySlottedMoraleAbilityId = GetMoraleBarData (i)
				local AbilityIcon = GetAbilityData(currentlySlottedMoraleAbilityId).iconNum
				local abilityId = GetAbilityData(currentlySlottedMoraleAbilityId).id
				WindowSetGameActionData ("MoraleWindow"..i, GameData.PlayerActions.DO_ABILITY, abilityId, L"")
			end
		end
	end
end

function MoraleCircle.Reset(flags)
	local MouseOverWindowName = SystemData.MouseOverWindow.name
	for i=1,4 do
		if MouseOverWindowName == "MoraleWindow"..i then
			if  flags == SystemData.ButtonFlags.SHIFT then
				WindowSetScale("MoraleWindow"..i,1)
				WindowSetAlpha("MoraleWindow"..i,1)	
			end
		end
	end
end

function MoraleCircle.OnMouseWheel(x, y, delta, flags)
	local MouseOverWindowName = SystemData.MouseOverWindow.name
	if flags == SystemData.ButtonFlags.SHIFT then
		for i=1,4 do
			if MouseOverWindowName == "MoraleWindow"..i then
				if (delta > 0) then      
					--d(L"Scroll Up on "..towstring(i))
					WindowSetScale("MoraleWindow"..i,WindowGetScale("MoraleWindow"..i)+0.1)       
				elseif (delta < 0) then
					WindowSetScale("MoraleWindow"..i,WindowGetScale("MoraleWindow"..i)-0.1)      
					--d(L"Scroll Down on "..towstring(i))   
				end
			end
		end
	end
end

function MoraleCircle.RightClick(flags)
	local MouseOverWindowName = SystemData.MouseOverWindow.name
	if not (flags == SystemData.ButtonFlags.SHIFT) then
		for i=1,4 do
			if  MouseOverWindowName == "MoraleWindow"..i then
				local M_Cdown,M_MaxCdown = GetMoraleCooldown (i)		
				local _, currentlySlottedMoraleAbilityId = GetMoraleBarData (i)
				local abilityId = GetAbilityData(currentlySlottedMoraleAbilityId).id
				local MData = Player.GetAbilityData (GetAbilityData(currentlySlottedMoraleAbilityId).id, Player.AbilityType.MORALE)
				local MoraleCalc = (tonumber(MoralePerC)/tonumber(GetMoralePercentForLevel(i)))*100
				local SmartChannel = MoraleCircle.GetSmartChannel()
				local LinkName = L"[Morale "..towstring(i)..L": "..towstring(MData.name)..L"]"
				local DoneLink = CreateHyperLink(L"0",LinkName, {229, 77, 255}, {} )
				if (MoraleCalc >= 99) and (M_Cdown <= ActionButton.GLOBAL_COOLDOWN ) then
						SendChatText(towstring(SmartChannel)..towstring(DoneLink)..L" - <LINK data=\"0\" text=\"READY!\" color=\"50,255,50\">", L"")
				else
					if M_Cdown > ActionButton.GLOBAL_COOLDOWN then MoraleCD = L" - "..towstring("<LINK data=\"0\" text=\"")..towstring(math.modf(M_Cdown))..L"s\" color=\"255,50,50\">" else MoraleCD = L"" end
					if MoraleCalc <= 99 then MoralePercent = L" - <LINK data=\"0\" text=\""..towstring(math.modf(MoraleCalc))..L"%\" color=\"255,255,50\">" else MoralePercent = L"" end
					SendChatText(towstring(SmartChannel)..towstring(DoneLink)..towstring(MoralePercent)..towstring(MoraleCD), L"")		
				end
			end
		end
		
	else
    EA_Window_ContextMenu.CreateContextMenu(SystemData.MouseOverWindow.name)   
    EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_SET_OPACITY ), EA_Window_ContextMenu.OnWindowOptionsSetAlpha, false, true )
    EA_Window_ContextMenu.Finalize()	
	end
end

function MoraleCircle.GetSmartChannel()
    local switcher = L"/say "
    local groupData = GetGroupData()
    for k,v in pairs(groupData) do
        if v and v.name and v.name ~= L"" then
            switcher = L"/party "
        end
    end
    if IsWarBandActive() and ((not GameData.Player.isInScenario) or (not GameData.Player.isInSiege)) then
        switcher = L"/warband "
    end
    if GameData.Player.isInScenario or GameData.Player.isInSiege then
        switcher = L"/sp "
    end    
    return switcher
end

function MoraleCircle.OnMoraleUpdated(moralePercent, moraleLevel)
	MoralePerC = math.abs(moralePercent)
end